#!/usr/bin/env bash
set -ex

# ---- BEGIN common ----
source "base/users.sh"
user_setup "gusted" "sudo"

source "base/base.sh"
setup_sshd

source "base/secrets.sh"
# ---- END common ----

# ---- BEGIN mCaptcha user ----
# Check if mcaptcha user, if not, create it.
if [ "$(id -u mcaptcha 2>/dev/null)" == "" ]; then
	adduser --disabled-password --gecos '' "mcaptcha"
fi
# ---- END mCaptcha user ----

# ---- BEGIN PostgreSQL ----
# Install PostgreSQL packages.
apt-get -y install postgresql postgresql-contrib

# Verify PostgreSQL is installed.
sudo -Hiu postgres psql -c 'SELECT version();'

# Setup mCaptcha database.
if [[ "$(sudo -Hiu postgres psql -c "SELECT 1 FROM pg_roles WHERE rolname='mcaptcha'")" == *"(0 rows)" ]]; then
	secret_get "MCAPTCHA_DB_PWD"
	sudo -Hiu postgres psql -c "CREATE USER mcaptcha WITH PASSWORD '${MCAPTCHA_DB_PWD}'"
fi
# ---- END PostgreSQL ----

# ---- BEGIN Redis ----
# Install Redis server.
apt-get -y install redis-server

# Install Cache module.
curl "https://dl.mcaptcha.org/mcaptcha/cache/master/cache-master-linux-amd64.tar.gz" -o /tmp/cache.tar.gz

# Unpack Cache module
tar -C /tmp -xf /tmp/cache.tar.gz cache-master-linux-amd64/libcache.so

# Make redis own the module.
chown redis:redis /tmp/cache-master-linux-amd64/libcache.so

# Move it into Redis' territory.
mv /tmp/cache-master-linux-amd64/libcache.so /etc/redis/libcache.so

# Tell Redis to load this module.
if [ "$(grep 'loadmodule /etc/redis/libcache.so' '/etc/redis/redis.conf')" == "" ]; then
	# Append load module to config.
	echo "loadmodule /etc/redis/libcache.so" >> /etc/redis/redis.conf
fi
# Make sure that redis is running on the latest version of mcaptcha-cache.
systemctl restart redis-server

# Verify redis is loaded.
redis-cli PING

# Clean up
rm /tmp/cache.tar.gz
rm -r /tmp/cache-master-linux-amd64
# ---- END Redis ----

# ---- BEGIN mCaptcha ----
# Install mCaptcha server.
curl "https://dl.mcaptcha.org/mcaptcha/mCaptcha/master/mcaptcha-master-linux-amd64.tar.gz" -o /tmp/mcaptcha.tar.gz

# Unpack Cache module
tar -C /tmp -xf /tmp/mcaptcha.tar.gz mcaptcha-master-linux-amd64/mcaptcha

# Move it into /usr/bin
mv /tmp/mcaptcha-master-linux-amd64/mcaptcha /usr/bin/mcaptcha

# Moving systemd service.
cp hosts/mcaptcha/conf/mcaptcha.service /lib/systemd/system/mcaptcha.service

# Crete mcaptcha directory.
mkdir -p /etc/mcaptcha

# Copy settings file.
cp hosts/mcaptcha/conf/mcaptcha-config.toml /etc/mcaptcha/config.toml

# Configure settings file.
secret_get "MCAPTCHA_COOKIE_SECRET"
secret_get "MCAPTCHA_SALT"

sed -i -e "s/TPL_COOKIE_SECRET/${MCAPTCHA_COOKIE_SECRET}/" -e "s/TPL_SALT/${MCAPTCHA_SALT}/" -e "s/TPL_DB_PWD/${MCAPTCHA_DB_PWD}/" /etc/mcaptcha/config.toml

# Start and enable mCaptcha service.
systemctl daemon-reload
systemctl stop mcaptcha
systemctl enable --now mcaptcha

# Hacky to wait until mCaptcha is listening on port 7000.
sleep 1

# Check if user exists, if not create one and register a site.
if [[ "$(curl -f -X 'POST' 'http://localhost:7000/api/v1/account/username/exists' -H 'accept: application/json' -H 'Content-Type: application/json' -d '{"val": "cborg"}')" == *"false"* ]]; then
	# Configure secret for admin password of mCaptcha instance.
	secret_get "MCAPTCHA_ADMIN_PWD"

	# Create user on mCaptcha user.
	curl -f -X POST -H "Content-Type: application/json" -d "{\"username\": \"cborg\", \"password\": \"$MCAPTCHA_ADMIN_PWD\", \"confirm_password\": \"$MCAPTCHA_ADMIN_PWD\", \"email\": \"noreply@codeberg.org\"}" http://localhost:7000/api/v1/signup

	# Get auth cookie and store it into /tmp/mcaptcha_cookies.
	curl -f -X POST --cookie-jar /tmp/mcaptcha_cookies -H "Content-Type: application/json" -d "{\"login\": \"cborg\", \"password\": \"$MCAPTCHA_ADMIN_PWD\"}" http://localhost:7000/api/v1/signin

	# Register domain at mCaptcha instance.
	SITE_KEY="$(curl -f -X POST -b /tmp/mcaptcha_cookies -H 'accept: application/json' -H 'Content-Type: application/json' -d '{"description": "codeberg.org", "avg_traffic": 50000, "peak_sustainable_traffic": 3000000, "broke_my_site_traffic": 5000000}' 'http://localhost:7000/api/v1/mcaptcha/add/easy')"

	# Get user secret.
	USER_SECRET="$(curl -f -b /tmp/mcaptcha_cookies -H 'accept: application/json' -H 'Content-Type: application/json' http://localhost:7000/api/v1/account/secret/get)"

	# Clean up.
	rm /tmp/mcaptcha_cookies
fi

# Disable registrations and move domain.
sed -i -e 's/allow_registration = true/allow_registration = false/' -e 's/domain = "localhost"/domain = "mcaptcha.codeberg.org"/' /etc/mcaptcha/config.toml

# Restart server.
systemctl restart mcaptcha

# Clean up.
rm /tmp/mcaptcha.tar.gz
rm -r /tmp/mcaptcha-master-linux-amd64
# ---- END mCaptcha ----

# If a domain was registered at mCaptcha this run, display the important info.
if ! [ -z ${USER_SECRET+x} ]; then
	echo "(i) User's secret: $USER_SECRET"
	echo "(i) Site key: $SITE_KEY"
fi

echo "Succesfully installed mCaptcha container!"
