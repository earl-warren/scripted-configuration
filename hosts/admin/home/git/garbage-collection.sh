#!/bin/bash

#set -euo pipefail

# Script to do some garbage collection experiments, by @fnetx
# Licenced under CC-0 (Public domain)

if [ $EUID -eq 0 ]; then
	echo "Script should run as git user"
	exit 1
fi

function get_repos () {
	# get repos that have either changed in the last two days (active repositories), or have changed some time ago (so we can check if objects should be pruned)
	nice -n 10 find /mnt/ceph-cluster/git/gitea-repositories/ -mindepth 2 -maxdepth 2 -type d -mtime +35 -mtime -37 | grep -v "quarantine"
	nice -n 10 find /mnt/ceph-cluster/git/gitea-repositories/ -mindepth 2 -maxdepth 2 -type d -mtime -2 | grep -v "quarantine"
}

get_repos | while read repo; do
	cd "$repo"
	echo $repo
	# gc.auto=1000
	nice -n 10 git -c gc.auto=100 -c pack.threads=8 -c gc.autoDetach=false gc --auto --prune=30.days.ago
	sleep 0.1
done
