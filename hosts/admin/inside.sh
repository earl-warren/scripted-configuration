#!/usr/bin/env bash

set -ex

source "base/users.sh"

if [ "$(id -u "git" 2>/dev/null)" == "" ]; then
	useradd --uid 1001 git
fi
mkdir -p /home/git
user_setup "otto" "sudo"
user_setup "ashimokawa" "sudo"

source "base/base.sh"
setup_sshd
enable_backports

apt-get install -y git

# temporary, for running the app directly inside the server
apt-get install -y --no-install-recommends make
apt-get install -y --no-install-recommends -t bullseye-backports golang-go

if [ ! -d "/root/admin-tool" ]; then
	git clone https://codeberg.org/codeberg/moderation /root/admin-tool
fi

## automatic garbage collection
install_file "admin" "/home/git/garbage-collection.sh"
source "base/systemd.sh"
systemd_timer "garbage collection" "daily" "/home/git/garbage-collection.sh" "git"
