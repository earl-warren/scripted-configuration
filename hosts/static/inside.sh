#!/usr/bin/env bash

set -ex

source "base/users.sh"

user_setup build
user_setup "otto" "sudo,build"
user_setup "ashimokawa" "sudo,build"
user_setup "gusted" "sudo,build"


source "base/base.sh"
setup_sshd

apt-get install -y --no-install-recommends lighttpd gettext-base

function create_site () {
	SUBDOMAIN="${1}"
	sudo -u www-data mkdir -p "/var/www/${SUBDOMAIN}/"
	chown "www-data:build" "/var/www/${SUBDOMAIN}"
	chmod 02770 "/var/www/${SUBDOMAIN}/"
	SUBDOMAIN="${SUBDOMAIN}" envsubst \$SUBDOMAIN > "/etc/lighttpd/conf-enabled/${SUBDOMAIN}.conf" < hosts/static/lighttpd-template.conf
}


create_site "design"

create_site "blog"
apt-get install -y --no-install-recommends pelican make
install_file "static" "/home/build/build_blog.sh"
chown "build:build" "/home/build/build_blog.sh"
source "base/systemd.sh"
systemd_timer "Update Blog" "hourly" "/home/build/build_blog.sh" "build"

create_site "docs"
create_site "fonts"
create_site "get-it-on"

systemctl reload lighttpd

