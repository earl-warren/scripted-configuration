# scripted-configuration

An attempt at a much more simple and intuitive configuration system (experimental / proof of concept)

**The problem:**

We're using [Ansible](https://codeberg.org/Codeberg-Infrastructure/configuration-as-code) for our configuration,
but our experience was anything but smooth.
The repo is often in an undeployable state,
there is not much you can do to test,
the workflow is very unflexible.

The result is that stuff is done alongside Ansible and checked in later,
often it is not.
We have several black boxes, and this is a problem.


**A solution attempt:**

So I aim for a much simpler workflow,
which is 

- easy to debug (bash / shell scripts in the end)
- flexible to use (config can be changed from within the container by updating the scripts inside)
- easy to getting started (in order to setup a new machine, you don't need to learn about Ansible, all you need to do is to run an interactive shell script)
- independent and not blocking your goal (you can use functions from within this repo, or tell the shell script to do something completely different)
- always ready to be run (no broken / out-of-sync state by making it easier to work with the tools on the system)


**Looking for help:**

We're looking for input at the approach,
as we're mainly testing the idea.

Feel free to get in touch to discuss other alternatives to Ansible,
approaches to system configuration,
or potentially improving this toolset to be more flexible, useful and joy to use.