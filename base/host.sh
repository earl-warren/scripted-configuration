#!/usr/bin/env bash

set -ex

function host_configure_container () {
	if [ -z "${CPU_MAX+x}" ]; then
		read -p "How many CPU cores should be made available? " CPU_MAX
		: ${CPU_MAX:=2}
	fi

	if [ -z "${MEM_HIGH+x}" ]; then
		read -p "How much memory should be made available, in GB? " MEM_HIGH
		: ${MEM_HIGH:=2}
	fi

	MEM_MAX=$((1024**3 * ${MEM_HIGH} * 6/5))
	MEM_HIGH=$((1024**3 * ${MEM_HIGH}))
	CPU_MAX=$((100000 * ${CPU_MAX}))
}
